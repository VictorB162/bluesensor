package com.example.ailab.bluesensor;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.UUID;

public class Userinterface extends AppCompatActivity {

    private static final String TAG = "Userinterface";

    Button IdDesconectar;
    TextView IdBufferIn;

    Handler bluetoothIn;
    final int handlerState = 0;
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private ConnectedThread MyConexionBT;
    private double nMessage = 0.0;
    private double nXmax = 100.0;

    private StringBuilder DataStringIN = new StringBuilder();
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static String address = null;

    LineGraphSeries<DataPoint> xySeries;
    GraphView mScatterPlot;
    private ArrayList<XYValue> xyValueArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userinterface);

        IdDesconectar = (Button) findViewById(R.id.IdDesconectar);
        IdBufferIn = (TextView) findViewById(R.id.IdBufferIn);
        /*
        bluetoothIn = new Handler() {
            public void handleMessage(android.os.Message msg) {
                if (msg.what == handlerState) {
                    String readMessage = (String) msg.obj;
                    IdBufferIn.setText(getText(R.string.temperatura) + " " + readMessage);
                    nMessage++;
                    double x = nMessage;
                    double y = Double.parseDouble(readMessage);
                    Log.d(TAG, "onClick: Adding a new point. (x,y): (" + x + "," + y + ")" );
                    xyValueArray.add(new XYValue(x,y));
                    initGraph();
                }
            }
        };
        */

        bluetoothIn = new Handler() {
            public void handleMessage(android.os.Message msg) {
                if (msg.what == handlerState) {
                    String readMessage = (String) msg.obj;
                    DataStringIN.append(readMessage);

                    int endOfLineIndex = DataStringIN.indexOf("\r\n");

                    if (endOfLineIndex > 0) {
                        String dataInPrint = DataStringIN.substring(0, endOfLineIndex);
                        IdBufferIn.setText(getText(R.string.temperatura) + dataInPrint);//<-<- PARTE A MODIFICAR >->->
                        DataStringIN.delete(0, DataStringIN.length());
                        nMessage++;
                        double x = nMessage;
                        double y = Double.parseDouble(dataInPrint);
                        Log.d(TAG, "onClick: Adding a new point. (x,y): (" + x + "," + y + ")" );
                        xyValueArray.add(new XYValue(x,y));
                        initGraph();
                    }
                }
            }
        };

        btAdapter = BluetoothAdapter.getDefaultAdapter();
        VerificarEstadoBT();

        IdDesconectar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (btSocket!=null)
                {
                    try {
                        btSocket.close();
                    }
                    catch (IOException e)
                    {
                        Log.e(TAG, "IdDesconectar.setOnClickListener: IOException: " + e.getMessage());
                    }
                }
                finish();
            }
        });

        mScatterPlot = (GraphView) findViewById(R.id.scatterPlot);
        xyValueArray = new ArrayList<>();

        initGraph();

    }

    @Override
    public void onResume()
    {
        super.onResume();
        Intent intent = getIntent();
        address = intent.getStringExtra(BTDevices.EXTRA_DEVICE_ADDRESS);
        BluetoothDevice device = btAdapter.getRemoteDevice( (String) address);

        try {
            btSocket = device.createRfcommSocketToServiceRecord(BTMODULEUUID);
        } catch (IOException e) {
            Log.e(TAG, "onResume: IOException: " + e.getMessage());
        }

        try {
            btSocket.connect();
        } catch (IOException e) {
            Log.e(TAG, "onResume: IOException: " + e.getMessage());
            try {
                btSocket.close();
            } catch (IOException e2) {
                Log.e(TAG, "onResume: IOException: " + e.getMessage());
            }
        }

        MyConexionBT = new ConnectedThread(btSocket);
        MyConexionBT.start();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        try {
            btSocket.close();
        } catch (IOException e2) {}
    }

    private void VerificarEstadoBT()
    {
        if(btAdapter==null) {
            Toast.makeText(getBaseContext(), "El dispositivo no soporta bluetooth", Toast.LENGTH_LONG).show();
        } else {
            if (btAdapter.isEnabled()) {
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
    }

    private void initGraph()
    {
        Log.d(TAG, "initGraph: Creating or refresh graph.");
        xySeries = new LineGraphSeries<>();

        if(xyValueArray.size() != 0){
            createLinearPlot();
        }else{
            Log.d(TAG, "onCreate: No data to plot.");
        }
    }

    private void createLinearPlot()
    {
        Log.d(TAG, "createLinearPlot: Creating scatter plot.");
        //xyValueArray = sortArray(xyValueArray);
        for(int i = 0;i <xyValueArray.size(); i++){
            try{
                double x = xyValueArray.get(i).getX();
                double y = xyValueArray.get(i).getY();
                xySeries.appendData(new DataPoint(x,y),true, 1000);
            }catch (IllegalArgumentException e){
                Log.e(TAG, "createLinearPlot: IllegalArgumentException: " + e.getMessage() );
            }
        }

        mScatterPlot.setDrawingCacheBackgroundColor(Color.GRAY);

        mScatterPlot.getViewport().setScalable(true);
        mScatterPlot.getViewport().setScalableY(true);
        mScatterPlot.getViewport().setScrollable(true);
        mScatterPlot.getViewport().setScrollableY(true);

        mScatterPlot.getViewport().setYAxisBoundsManual(true);
        mScatterPlot.getViewport().setMaxY(50);
        mScatterPlot.getViewport().setMinY(10);

        mScatterPlot.getViewport().setXAxisBoundsManual(true);
        if(nMessage < nXmax){
            mScatterPlot.getViewport().setMinX(-0.5);
            mScatterPlot.getViewport().setMaxX(nXmax + 0.5);
        }else{
            mScatterPlot.getViewport().setMinX(-0.5 + nMessage - nXmax);
            mScatterPlot.getViewport().setMaxX(nMessage + 0.5);
        }


        mScatterPlot.addSeries(xySeries);
    }

    private class ConnectedThread extends Thread
    {
        private final InputStream mmInStream;

        public ConnectedThread(BluetoothSocket socket)
        {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            try
            {
                tmpIn = socket.getInputStream();
            } catch (IOException e) { }
            mmInStream = tmpIn;
        }

        public void run()
        {
            byte[] buffer = new byte[256];
            int bytes;

            while (true) {
                try {
                    bytes = mmInStream.read(buffer);
                    String readMessage = new String(buffer, 0, bytes);
                    // Envia los datos obtenidos hacia el evento via handler
                    bluetoothIn.obtainMessage(handlerState, bytes, -1, readMessage).sendToTarget();

                } catch (IOException e) {
                    break;
                }
            }
        }
    }

}